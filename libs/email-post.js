$(function(){

  var formSubmits = 0;
  $('.clickGa').submit(function(){
    event.preventDefault();
    if (formSubmits !== 0) {
      return false;
    }
    formSubmits++;
    var $form = $(this);
    var email = fetchEmailAddress($form);
  });

  var emailPost = function (data,success,error) {

    var url = "https://exp-email.herokuapp.com/";
    $.ajax({
      type: "POST",
      url: url,
      data: $.extend(data, {"type":"subscribed"}),
      success: success,
      error: error
    });
  }

});

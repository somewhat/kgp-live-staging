var currWidth = window.innerWidth;
$("#sliderWrap, .sliderList, .slider-slide, .sliderList .bestfit-slider-image").css({"height": window.innerHeight});	
function setHomeSlider(){	
	$("#sliderWrap, .sliderList, .slider-slide, .sliderList .bestfit-slider-image").css({"height": window.innerHeight,"opacity":1});
	if($(window).width() <= 767){
		$('.sliderList .bestfit-slider-image').each(function(index, element) {
            $(this).css({'background-image':'url('+$(this).attr('mob-data')+')'});
        });
	}else {
		$('.sliderList .bestfit-slider-image').each(function(index, element) {
            $(this).css({'background-image':'url('+$(this).attr('des-data')+')'});
        });
	}
	$('.sliderList, .running-slider-inner').cycle('destroy');
	$('.sliderList').cycle({ 
		fx:    'scrollHorz', 
		speed:  1000,
		next:   '.sliderList-next', 
		prev:   '.sliderList-prev', 
		timeout:  5000, 
		startingSlide: Math.floor(Math.random()*$('.sliderList .slider-slide, .running-slider-inner .running-slider').length)
	});
  $('.running-slider-inner').before('<div id="nav">').cycle({ 
    fx:    'scrollHorz', 
    speed:  1000,
    next:   '.running-sliderList-next', 
    prev:   '.running-sliderList-prev', 
    timeout:  5000,
    pager:  '#nav'
  });

}
setTimeout(function(){ 
	setHomeSlider();
}, 3000);
$( window ).load(function() {

	currWidth = window.innerWidth;
	setTimeout(function(){ 
		$(".toHtml").each(function(index, element) {
			var res = $(this).html().replace(/&lt;/g , "<");
			var res = res.replace(/&gt;/g, ">");
			$(this).html(res).css({'opacity':1});
			/*$('body').css({"opacity":1});*/
		});
		$("#partner-main li").each(function(index, element) {

      //var nameVal = getUrlVar($(this).find('.partner-url').attr('href'))["name"];
      
      var syr = $(this).find('.partner-name').html().replace(" ", "-");
      var syre = syr.replace("&nbsp;", "-");
      //console.log(syre.replace(" ", "-"));
      $(this).find('.partner-url').attr('href',$(this).find('.partner-url').attr('href')+syre.replace(" ", "-"))
			if($(this).find('.our-partenrs-block').attr('name') == 'Natalie'){
				$(this).clone().appendTo("#partner-top");
				$(this).remove();
			}
			if($(this).find('.our-partenrs-block').attr('name') == 'Margo'){
				$(this).clone().appendTo("#partner-top");
				$(this).remove();
			}
      if($(this).find('.our-partenrs-block').attr('name') == 'Lindsey'){
        $(this).clone().appendTo("#partner-top");
        $(this).remove();
      }
		});
    $(".read-link").each(function(index, element) {
      if($(this).attr('href') == 'http:' || $(this).attr('href') == ''){
        $(this).remove(); 
      }
    });
	}, 500);
	$("#partner-main li").each(function(index, element) {
		if($(this).find('.our-partenrs-block').attr('name') == 'Natalie'){
			$(this).clone().appendTo("#partner-top");
			$(this).remove();
		}
		if($(this).find('.our-partenrs-block').attr('name') == 'Margo'){
			$(this).clone().appendTo("#partner-top");
			$(this).remove();
		}
	});
	$(".news-single").text(function(index, currentText) {
			return currentText.substr(0, 412)+'....';
		});
		$(".read-link").each(function(index, element) {
			if($(this).attr('href') == 'http:' || $(this).attr('href') == ''){
				$(this).remove();	
			}
		});
		
	/*var $item = $('#myCarousel.carousel .item');
	var $numberofSlides = $('#myCarousel .item').length;
	var $currentSlide = Math.floor(Math.random() * $numberofSlides);
	
	$('#myCarousel .carousel-indicators li').hide();
	$('#myCarousel .carousel-indicators li').each(function () {
	var $slideValue = $(this).attr('data-slide-to');
	if ($currentSlide == $slideValue) {
	  $(this).addClass('active');
	  $item.eq($slideValue).addClass('active');
	} else {
	  $(this).removeClass('active');
	  $item.eq($slideValue).removeClass('active');
	}
	});*/
	//setHomeSlider();
	 $(".bestfit-slider-image, #myCarousel .carousel-inner").css("height", currWindowHeight);
  $("#myCarousel .carousel-inner").css("min-height", currWindowHeight);
	
  	if (currWidth < 768) {
		$("#myCarousel .item .bestfit-slider-image").each(function () {
		  currMobImg = $(this).attr("mob-data");
		  currBGURL = "url(" + currMobImg + ")";
		  $(this).css("background-image", currBGURL);
		});
		//mobile view our portfolio slider function
		$(".mob-portfolio").owlCarousel({
		  loop: true,
		  margin: 30,
		  responsiveClass: true,
		  slideBy: 1,
		  items: 1,
		  nav: true
		});
	  }
	  
	// Running Start images slider
  var $item_rs = $('#rsCarousel.carousel .running-slider');
  var $numberofSlides_rs = $('#rsCarousel .running-slider').length;
  var $currentSlide_rs = Math.floor(Math.random() * $numberofSlides_rs);

  $('#rsCarousel .carousel-indicators li').each(function () {
    var $slideValue_rs = $(this).attr('data-slide-to');
    if ($currentSlide_rs == $slideValue_rs) {
      $(this).addClass('active');
      $item_rs.eq($slideValue_rs).addClass('active');
    } else {
      $(this).removeClass('active');
      $item_rs.eq($slideValue_rs).removeClass('active');
    }
  });
  
  //Our portfolio section
  if (window.innerWidth > 991) {
    $(".industries-btn").addClass("expand");
  }
  $(".industries-btn").on('click', function () {
    $('.function-btn').removeClass("expand");
    $(this).toggleClass("expand");
    $('.function-menu-title').slideUp();
    if ($('.industries-menu-title').is(':visible')) {
      $('.industries-menu-title').slideUp();
      $('.our-portfolio-third-li-border-top').show();
    } else {
      $('.industries-menu-title').slideDown();
      $('.our-portfolio-third-li-border-top').show();
    }
  });
  $(".industries-menu-title li a").on("click", function (e) {
	$('.content-ourportfolio').html($(this).attr('other-desc'));
    e.preventDefault();
    e.stopPropagation();
    var icons = $(this);
    $('.icon-cat').hide();
    var iicon = icons.data('rel');
    $("." + iicon).show();
    $(".industries-menu-title li a").removeClass("active");
    $(this).addClass("active");
    if (window.innerWidth < 768) {
      //$('.portfolio-submenu').slideUp();
      //$('.industries-btn').removeClass("expand");
    }
  });
  $(".industries-menu-title li:first-child a").trigger('click');
  $(".function-btn").on('click', function () {
    $('.industries-btn').removeClass("expand");
    $(this).toggleClass("expand");
    $('.industries-menu-title').slideUp();
    if ($('.function-menu-title').is(':visible')) {
      $('.function-menu-title').slideUp();
      $('.our-portfolio-third-li-border-top').show();
    } else {
      $('.function-menu-title').slideDown();
      $('.our-portfolio-third-li-border-top').hide();
    }
  });
  $(".function-menu-title li a").on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();
    var ficons = $(this);
    $('.icon-cat').hide();
    var fiicon = ficons.data('rel');
    $("." + fiicon).show();
    $(".function-menu-title li a").removeClass("active");
    $(this).addClass("active");
    if (window.innerWidth < 768) {
      //$('.portfolio-submenu').slideUp();
      //$('.function-btn').removeClass("expand");
    }
  });

  $(".industries-btn").click(function (e) {
    e.preventDefault();
    //e.stopPropagation();
    var xicons = $(this);
    $('.icon-cat').hide();
    var xiicon = xicons.data('rel');
    $("." + xiicon).show();
    $('.ind-clients').show();
  });
  	
 
  
  $('.function-menu-title, .function-btn').click(function () {
    $('.our-portfolio-right-desc').hide();
  });
  $('.industries-btn').click(function () {
    $('.our-portfolio-right-desc').show();
  }); 
  // global-location owl Carousel
  // $('.global-location').owlCarousel({
  //   loop: true,
  //   margin: 30,
  //   responsiveClass: true,
  //   transitionStyle: "fade",
  //   stopOnHover: false,
  //   slideBy: 1,
  //   responsive: {
  //     0: {
  //       items: 1,
  //       nav: true,
  //       loop: true,
  //       margin: 0
  //     },
  //     767: {
  //       items: 2,
  //       autoplay: true,
  //       autoplayTimeout: 4000,
  //       nav: true,
  //       loop: false
  //     },
  //     1199: {
  //       items: 3,
  //       loop: true,
  //       nav: true
  //     }
  //   }
  // }); 
  $(".stand-menu-title li a").click(function (e) {
    e.preventDefault();
    var sf = $(this);
    var tabID = "#" + $(this).attr("data-img");
    $(".tab-bg-img").css("top", "-20000px");
    $(tabID).css("top", "0px");
    $('.sf-desc').hide();
    var standFor = sf.data('rel');
    $("." + standFor).show();
    $(".stand-menu-title li a").css("opacity", "0.4");
    $(".stand-menu-title li a span").css("display", "none");
    $(this).css("opacity", "1");
    $(this).find("span").css("display", "block");
    var currTabContainer = $(".tab-bg-main .container").outerHeight();
    $(".tab-bg-img, #stand-for-synchronous-fit").css("height", currTabContainer);
  });
  $(".stand-menu-title li:first-child a").trigger('click');
  
  // NEWS section
  
  $(".news-pagination li a").click(function (e) {
    e.preventDefault();
    var newsBlock = $(this);
    $('.news-block').hide();
    var newsC = newsBlock.data('rel');
    $("." + newsC).show();
  });

  $(".news-single").text(function (index, currentText) {
    return currentText.substr(0, 412) + '....';
  });
  
 

  var showChar = 390;
  var ellipsestext = "...";
  var moretext = "Read more ";
  var lesstext = "Read less";

  $('.read-more-collapse').each(function () {
    var content = $(this).html();

    if (content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar, content.length - showChar);

      var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="news-read-more white">' + moretext + '</a></span>';

      $(this).html(html);
    }
  });
	$(document).on('click', '.news-read-more', function (event) {
		if ($(this).hasClass("less")) {
		  $(this).removeClass("less");
		  $(this).html(moretext);
		} else {
		  $(this).addClass("less");
		  $(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	  });
	 var news_count = $('.news-list-main').length;
  var count = 0;
  var rel_count = 1;
  if (news_count > 0) {
    for (i = 1; i <= news_count; i++) {
      $('<li class="news-page' + i + '" rel="li' + rel_count + '" />').text(i).appendTo('.news-pagination');
      count++;
      if (count > 2) {
        rel_count++;
        count = 0;
      }
    }
  }
  $('.news-pagination li:first-child').addClass("active");
  $('.news-pagination li').click(function () {
    $('.news-description-main li').hide();
    $('.news-pagination li').removeClass("active");
    $(this).addClass("active");
    $('.news-description-main li').eq($(this).index()).css("display", "inline-block");
  });
  var click = 1;
  var min_click = 1;
  var max_click = $('.news-pagination li').length / 3;
  $('.news-pagination-next').click(function () {
    $('.news-pagination li').hide();
    click++;
    $('.news-pagination li').each(function () {
      if ($(this).attr('rel') == 'li' + click) {
        $(this).show();
      }
    });

    if (click > min_click) {
      $('.news-pagination-pre').css("display", "inline-block");
    } else {
      $('.news-pagination-pre').hide();
    }
    if (click < max_click) {
      $('.news-pagination-next').css("display", "inline-block");
    } else {
      $('.news-pagination-next').hide();
    }
  });
  $('.news-pagination-pre').click(function () {

    $('.news-pagination li').hide();
    click--;
    $('.news-pagination li').each(function () {
      if ($(this).attr('rel') == 'li' + click) {
        $(this).show();
      }
    });
    if (click > min_click) {
      $('.news-pagination-pre').css("display", "inline-block");
    } else {
      $('.news-pagination-pre').hide();
    }
    if (click < max_click) {
      $('.news-pagination-next').css("display", "inline-block");
    } else {
      $('.news-pagination-next').hide();
    }
  });
});
$(document).ready(function () {
	setTimeout(function(){ 
		$(".toHtml").each(function(index, element) {
			var res = $(this).html().replace(/&lt;/g , "<");
			var res = res.replace(/&gt;/g, ">");
			$(this).html(res);
			$('body').css({"opacity":1});
		});
		
	}, 500);
  

  //Page banner update mobile & ipad //Tab bg images update for mobile & ipad 
  var commonPageBanner = ".page-banner-mob-ipad";
  commonMobileIpadImageUpdate(commonPageBanner);
  var tabBGImage = ".tab-bg-main .tab-bg-mobile-ipad";
  commonMobileIpadImageUpdate(tabBGImage);
  function commonMobileIpadImageUpdate(event) {
    $(event).each(function () {
      if (window.innerWidth < 768) {
        currMobImg = $(this).attr("data-mobile");
        currBgURL = "url(" + currMobImg + ")";
        $(this).css("background-image", currBgURL);
      }
      if (window.innerWidth >= 768 && window.innerWidth <= 991) {
        currIpadImg = $(this).attr("data-ipad");
        currBgURL = "url(" + currIpadImg + ")";
        $(this).css("background-image", currBgURL);
      }
    });
  }


  //synchronous page background fixing:
  var tabAreaHeight = 0;
  setTimeout(function () {
    $("#stand-for-synchronous-fit .sf-desc").each(function () {
      var h = $(this).height();
      if (h > tabAreaHeight) {
        tabAreaHeight = h;
      }
    });
    if (currWidth > 992) {
      $("#stand-for-synchronous-fit .tab-area-outer").css("height", tabAreaHeight + 25);
    }
  }, 500);
  currWindowHeight = window.innerHeight;
  currTabContainer = $(".tab-bg-main").height();
  
 
  var $form = $(this).closest('form');
  $(".input-single input,.input-single textarea").on('keyup touchend', function(){
    $(this).parent().find("label").css("display", "none");
    if ($(this).val() == '') {
      $(this).parent().find("label").show();
    }
  });
  $("#contact-code").on('keyup touchend', function(){
    $(".country-code-label").css("display", "none");
    if ($(this).val() == '') {
      $(".country-code-label").show();
    }
  });
  $("#contact-tel").on('keyup touchend', function(){
    $(".tel-cod-label").css("display", "none");
    if ($(this).val() == '') {
      $(".tel-cod-label").show();
    }
  });
  $("#contact-tel").on('keyup touchend', function(e){
    //if (e.keyCode == 8) {
      if ($(this).val().length < 1) {
        $('#contact-code').focus();
      }
    //}
		if ($("#contact-tel").val().length >= 10) {
      $("#contact-tel").addClass("validate");
    } else {
      $("#contact-tel").removeClass("validate");
    }
  });
  $("#contact-code").on('keyup touchend', function(){
    var count = $(this).val().length;
    $(this).parents(".input-single").find("label").css("display", "none");
    if ($(this).val() == '') {
      $(this).parents(".input-single").find("label").show();
    }
    if (count == 4) {
      $('#contact-tel').focus();
    }
  });
  $(".input-single select").change(function () {
    $(this).parent().find("label").hide();
    $(this).css("color", "#5f51cb").addClass("validate");
    if ($(this).val() == '') {
      $(this).parent().find("label").show();
      $(this).css("color", "#5f51cb").removeClass("validate");
    }
  });
  $(".input-single input.company-name,.input-single input.text-input").on('keyup touchend', function(){
    if ($(this).val().length > 0) {
      $(this).css("color", "#5f51cb").addClass("validate");
    } else {
      $(this).css("color", "#ddd").removeClass("validate");
    }
		
  });
  $("#contact-tel, #contact-code").keypress(function (e) {

    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }

		/*if ($("#contact-tel").val().length >= 10) {
      $("#contact-tel").addClass("validate");
    } else {
      $("#contact-tel").removeClass("validate");
    }*/
  });
  /*$(".telephone-main input.telephone").on('keyup touchend', function(){
    if ($(this).val().length >= 10) {
      $(this).addClass("validate");
    } else {
      $(this).removeClass("validate");
    }
  });*/

  function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
  };
  $("#contact-mail").on('keyup touchend', function(){
    validateForm();
    if (isValidEmailAddress($(".input-single input.email").val())) {
      $(this).css("color", "#5f51cb").addClass("validate");
    } else {
      $(this).removeClass("validate");
    }
  });
  $("#contact-mail").blur(function () {
    if (isValidEmailAddress($(".input-single input.email").val())) {
      $(this).css("color", "#5f51cb").addClass("validate");
    } else {
      $(this).removeClass("validate");
    }
  });
  $("#fromemail, #name").on('keyup touchend', function(){
    validatepartnerForm();
    if (isValidEmailAddress($("#fromemail").val())) {
      $("#fromemail").css("color", "#5f51cb").addClass("validate");
    } else {
      $("#fromemail").removeClass("validate");
    }
  });
  $("#fromemail").blur(function () {
    if (isValidEmailAddress($("#fromemail").val())) {
      $(this).css("color", "#5f51cb").addClass("validate");
    } else {
      $(this).removeClass("validate");
    }
    validatepartnerForm();
  });
  $("#name").blur(function () {
    validatepartnerForm();
  });
  var validateparnter = false;
  function validatepartnerForm() {

    if ($('#name').hasClass('validate') && $('#fromemail').hasClass('validate')) {
      validateparnter = true;
    } else {
      validateparnter = false;
    }
    if (validateparnter) {
      $('.contact-submitpartner .btn').prop('disabled', false);
    } else {
      $('.contact-submitpartner .btn').prop('disabled', true);
    }
  }
  $("#contactModal input").blur(function () {
    validateForm();
  });
  var validate = false;
  function validateForm() {
    if ($('#contactname').hasClass('validate') && $('#contact-mail').hasClass('validate') && $('#contact-tel').hasClass('validate')) {
      validate = true;
    } else {
      validate = false;
    }
    if (validate) {
      $('#contact-form-send').prop('disabled', false);
    } else {
      $('#contact-form-send').prop('disabled', true);
    }
  }

  $(".global-menu-title li a").click(function (e) {
    e.preventDefault();
    var gmt = $(this);
    $('.world-map-row').hide();
    var globalMenuT = gmt.data('rel');
    $("." + globalMenuT).show();
  });

  

  

  

  var $ilist = $(".industries-menu-title");
  $ilist.children().detach().sort(function (a, b) {
    return $(a).text().localeCompare($(b).text());
  }).appendTo($ilist);
  $(".industries-menu-title li:first-child a").addClass("active");
  var $flist = $(".function-menu-title");
  $flist.children().detach().sort(function (a, b) {
    return $(a).text().localeCompare($(b).text());
  }).appendTo($flist);
  
  
  $('.partner-url').each(function () {
    var str = $(this).attr('href');
    var res = str.replace(/ /gi, "-").toLowerCase();
    $(this).attr('href', res + '.html');
  });
  $('#email-send').click(function () {
    var name = $("#name").val();
    $('.partnername-form').hide();
    var fromemail = $("#fromemail").val();
    var message = $("#message").val();
    $('.sucessmessage').show();
    $('.contact-submitpartner .btn').prop('disabled', true);

    //var datastr ='name=' + name + '&fromemail=' + fromemail + '&message=' + message + '&toemail=' + toemail;
    var datastr = JSON.stringify({ 'name': name, 'fromemail': fromemail, 'message': message, 'toemail': toemail });
    $.ajax({
      type: "POST",
      url: "https://kgp-mailer.herokuapp.com/send",
      data: datastr,
      cache: false,
      success: function success(html) {
        $('.sucessmessage').show();
      }
    });

    $('.partnerfeedback-form').hide();
  });

  $('#contact-form-send').click(function () {
    var contactname = $("#contactname").val();
    var companyname = $("#companyname").val();
    var contacttel = $("#contact-tel").val();
    var contactcode = $('#contact-code').val();
    var contactmail = $("#contact-mail").val();
    var select_partner = $("#select-partner").val();
    var contactdetails = $("#contact-details").val();
    $('.contactsucessmessage').show();
    $('.contact-submitpartner .btn').prop('disabled', true);

    var datastr = JSON.stringify({ 'contactname': contactname, 'companyname': companyname, 'contacttel': contacttel, 'contactcode': contactcode, 'contactmail': contactmail, 'select_partner': select_partner, 'contactdetails': contactdetails });
    $.ajax({
      type: "POST",
      url: "https://kgp-contactus.herokuapp.com/send",
      data: datastr,
      cache: false,
      success: function success(html) {
        $('.sucessmessage').show();
      }
    });

    $('.contact-page-form').hide();
  });
  
  /*Interview mail*/
  $("#assesmentModal input").blur(function(){
    validateForm1();
  });
  var validate1 = false;
  function validateForm1(){
    if( $("#interviewfname").val() != '' && $("#interviewlname").val() != '' &&  $("#interviewcname").val() != '' && $("#interviewtitle").val() != '' && isValidEmailAddress($('#interviewemail').val()) &&  $('.tc-checkbox').prop("checked") == true ){
       validate1 = true
    }else{
        validate1 = false
      }
    if(validate1){
      $('#interview-form-send').prop('disabled', false);
    }else{
      $('#interview-form-send').prop('disabled', true);
    }
  }
$(".interview-page-form .input-single input").on('keyup touchend', function(){
   $(this).parent().find("label").hide();
   $(this).css("color","#5f51cb ").addClass("validate");
   if($(this).val()== ''){
     $(this).parent().find("label").show();
     $(this).css("color","#5f51cb ").removeClass("validate");
   }
 });
 $('.tc-checkbox').click(function () {
    validateForm1();
  });
$("#interviewemail").on('keyup touchend', function(){
	validateForm1();
	if (isValidEmailAddress($("#interviewemail").val())) {
	  $(this).css("color", "#5f51cb").addClass("validate");
	} else {
	  $(this).removeClass("validate");
	}
});
$("#interviewemail").blur(function () {
	if (isValidEmailAddress($("#interviewemail").val())) {
	  $(this).css("color", "#5f51cb").addClass("validate");
	} else {
	  $(this).removeClass("validate");
	}
});
  $('#interview-form-send').click(function(){
    var interviewfname = $("#interviewfname").val();
    var interviewlname = $("#interviewlname").val();
    var interviewcname = $("#interviewcname").val();
    var interviewtitle = $('#interviewtitle').val();
    var interviewemail = $("#interviewemail").val();
    var interviewlinkedinurl = $("#interviewlinkedinurl").val();
    $('.interviewsucessmessage').show();
    // $('.interview-submit .btn').prop('disabled', true);

    var datastr = JSON.stringify({'interviewfname': interviewfname, 'interviewlname': interviewlname, 'interviewcname': interviewcname,'interviewtitle': interviewtitle, 'interviewemail': interviewemail, 'interviewlinkedinurl': interviewlinkedinurl});
    $.ajax({
      type: "POST",
      url: "https://kgpinterview.herokuapp.com/send",
      data: datastr,
      cache: false,
      success: function(html){
        $('.interviewsucessmessage').show();
      }
    });

    $('.interview-page-form').hide();
  })

  $('#closeform-partner').click(function () {
    $('.partnerfeedback-form,.partnerfeedback-form .input-single label').show("slow");
    $('.sucessmessage').hide();
    $('#name,#fromemail,#message').val('').removeClass("validate");
  });
  var toemail;
$(document).on('click', '.email-partner-click', function (event) {
//  $('.email-partner-click').click(function () {
    var partner_name = $(this).parents('.our-partenrs-block').find('h2').text();
    $('.partnername-form').html('Connecting to ' + partner_name);
    toemail = $(this).data('mailid');
  });
$(document).on('click', '.individual-page-email-link', function (event) {  
//  $('.individual-page-email-link').click(function () {
    var partner_name = $(this).parents('.partner-details').find('h4').text();
    $('.partnername-form').html('Connecting to ' + partner_name);
    toemail = $(this).data('mailid');
  });
  var partnerblock_length = $('.our-partenrs-block').length;
  if (partnerblock_length > 20) {
    $('#page_navigation').show();
  } else {
    $('#page_navigation').hide();
  }

  //Our partner list equal height fixing action
  var maxHeight = 0;
  $(".our-partenrs-block .partner-url").each(function () {
    if ($(this).height() > maxHeight) {
      maxHeight = $(this).height();
    }
  });
  $(".our-partenrs-block .partner-url").height(maxHeight);

  $('#select-partner option[value="James W. Thompson"]').text("James W. Thompson, Ph.D.");

  


});

$(window).scroll(function () {
  var sticky = $('.header-nav');
  scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass('headerAnimation');else sticky.removeClass('headerAnimation');
});

//contact section partner dropdown filter by alphabet action start
function contactPartnerListFilter() {
  var selectPartner = document.getElementById('select-partner');
  var filterArray = [];

  for (i = 1; i < selectPartner.length; i++) {
    filterArray[i - 1] = selectPartner.options[i].text.toUpperCase() + "," + selectPartner.options[i].text + "," + selectPartner.options[i].value;
  }

  filterArray.sort();

  for (i = 1; i < selectPartner.length; i++) {
    var parts = filterArray[i - 1].split(',');
    selectPartner.options[i].text = parts[1];
    selectPartner.options[i].value = parts[2];
  }
}
$(window).resize(function(){
	setHomeSlider()	
});

